Note about the assignment:
Because of time constraints caused by a personal conflict last week (I came and spoke to you during your office hours), I worked on this assignment alone.
I wasn't able to get the implementations of the algorithms working, but I attached my attempts in python scripts.
However, I still wanted to receive results, so I used built-in functions in R to obtain the results from the data talked about in my report.

"R CMD BATCH src.R"

on the command line. This preprocesses the data and visualizes the clustering results.