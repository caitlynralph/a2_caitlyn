#A template for the implementation of K-Medoids.
import math #sqrt


# Accepts two data points a and b.
# Returns the distance between a and b.
# Note that this might be specific to your data.
def Distance(a,b):
	dist = math.sqrt((a[0]-b[0])**2 + (a[1]-b[1])**2)
	return dist

# Accepts a list of data points D, and a list of centers
# Returns a dictionary of lists called "clusters", such that
# clusters[c] is a list of every data point in D that is closest
#  to center c.
# Note: This can be identical to the K-Means function of the same name.
def assignClusters(D, centers):
	clusters = {}
	for p in D:
		nearest = (D[p],centers[0])
		nearestCenter = 0
		for c in centers:
			if Distance(D[p],centers[c]) < nearest:
				nearest = (D[p],centers[c])
				nearestCenter = centers[c]
		clusters[nearestCenter].append((D[p],centers[c]))
	return clusters

# Accepts a list of data points, and a number of clusters.
# Produces a set of lists representing a K-Medoids clustering
#  of D.
def KMedoids(D, k):
	centers = D[1:k]
	while centers != oldCenters:
		oldCenters = centers
		clusters = assignClusters(D, centers)
		for c in centers:
			for cand in cluster[c]:
				dist_c = 0
				dist_cand = 0
				for point n clusters[c]:
					dist_c =+ Distance(c, point)
					dist_cand =+ Distance(cand, point)
				if dist_cand < dist_c:
					c = cand
