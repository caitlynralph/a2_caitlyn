#A template for the implementation of K-Means.
import math #sqrt


# Accepts two data points a and b.
# Returns the euclidian distance
# between a and b.
def euclidianDistance(a,b):
	dist = math.sqrt((a[0]-b[0])**2 + (a[1]-b[1])**2)
	return dist

# Accepts a list of data points D, and a list of centers
# Returns a dictionary of lists called "clusters", such that
# clusters[c] is a list of every data point in D that is closest
#  to center c.
def assignClusters(D, centers):
	clusters = []
	for p in D:
		nearest = (D[p],centers[0])
		nearestCenter = 0
		for c in centers:
			if euclidianDistance(D[p],centers[c]) < nearest:
				nearest = (D[p],centers[c])
				nearestCenter = centers[c]
		clusters[nearestCenter].append((D[p],centers[c]))
	return clusters

# Accepts a list of data points.
# Returns the mean of the points.
def findClusterMean(cluster):
	return math.mean(cluster)

# Accepts a list of data points, and a number of clusters.
# Produces a set of lists representing a K-Means clustering
#  of D.
def KMeans(D, k):
	means = D[0:k]
	clusters = assignClusters(D,means)
