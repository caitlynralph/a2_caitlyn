#A template for the implementation of hclust
import math #sqrt


# Accepts two data points a and b.
# Returns the distance between a and b.
# Note that this might be specific to your data.
def Distance(a,b):
	dist = math.sqrt((a[0]-b[0])**2 + (a[1]-b[1])**2)
	return dist

# Accepts two data points a and b.
# Produces a point that is the average of a and b.
def merge(a,b)
	p = []
	p[0] = mean(a[0],b[0])
	p[1] = mean(a[1],b[1])
	return p

# Accepts a list of data points.
# Returns the pair of points that are closest
def findClosestPair(D):
	closest = []
	minDist = Distance(D[0],D[1])
	for p in D:
		for i in length(D):
			if p == D[i]:
				pass
			else:
				if Distance(p,D[i]) < minDist:
					closest = [p,D[i]]
	return closest

# Accepts a list of data points.
# Produces a tree structure corresponding to a
# Agglomerative Hierarchal clustering of D.
def HClust(D):
	centers = D[1:n]
	splits = {}
	while length(centers) > 1:
		for c in centers:
			closest = findClosestPair(centers)
			centers -= closest[1]
			root = merge(closest)
			centers[c] = root
			splits[centers[c]] = closest
